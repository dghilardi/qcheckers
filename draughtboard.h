/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRAUGHTBOARD_H
#define DRAUGHTBOARD_H
#include <QBitArray>
#include <QList>
#include "chessbox.h"

class Draughtboard
{
public:
    Draughtboard();
    Draughtboard(QString base64);
    void setBase64(QString base64);
    void copyState(QList<ChessBox *> * draughtboardState);
    int boxState(int position);
    void setBoxState(int state, int position);
    bool isBlack(int position);
    QString toString();
    QString toBase64();
    int size();

private:
    QBitArray draughtboard;

    int toIndex(int position);
    int abs(int num);
    QByteArray bitsToBytes(QBitArray bits);
    QBitArray bytesToBits(QByteArray bytes);
};

#endif // DRAUGHTBOARD_H
