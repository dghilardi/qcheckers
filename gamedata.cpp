//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "gamedata.h"
#include "minimaxnode.h"
#include <QDebug>
#include<QTime>
#include"Sleeper.h"
const int BOX_ROWS = 8;
const int BOX_COLS = 8;


void GameData::resetBox(int index)
{
    int i = index/BOX_COLS;
    int j = index%BOX_COLS;
    ChessBox *createdBox = getBox(index);
    if((i+j)%2==0) {
        createdBox->setBlackBox(true);
        if(i<3) {
            createdBox->setState(1);
            createdBox->setFlipped(true);
        }else{
            if(i>4){
                /*if(i==5) createdBox->setState(-2); else*/ createdBox->setState(-1);
                createdBox->setFlipped(true);
            }else{
                createdBox->setState(0);
                createdBox->setFlipped(false);
            }
        }
    }else{
        createdBox->setBlackBox(false);
    }
}

GameData::GameData(QObject *parent)
    : QObject(parent)
{

    // Create boxes
    for(int i = 0; i < BOX_ROWS*BOX_COLS; i++) {
            m_boxes << new ChessBox;
            resetBox(i);
    }
    m_gameState=0;
    m_depth=7;
    gameLogic = new GameLogic(&m_boxes, BOX_ROWS, BOX_COLS);
    turn = -1;
    vsHuman=false;

    //MinimaxNode node("QIAcSAAAAxAAA7BhAA==");
    //node.getValue(3,false,-26,26);

}

GameData::~GameData()
{
}

void GameData::newGame(){
    setGameState(0);
    for(int i=0; i<m_boxes.length(); i++){
        resetBox(i);
    }
    movHistory.clear();
}

void GameData::doModifications(QList<qint8> myModif)
{
    QList<qint8> movement;
    //movHistory.append(myModif);
    //qDebug() << movHistory;
    //qDebug() << myModif;
    int doubleEaten=0;
    int pieceMoved = m_boxes.at(myModif.at(0))->getVisibleState();
    moveItem(myModif.at(0),myModif.at(1));
    for(int i=2; i<myModif.length();i++){
        int state = m_boxes.at(myModif.at(i))->getVisibleState();
        if(state==2||state==-2){
            ++doubleEaten;
            movement << myModif.at(i);
        }
        else movement.prepend(myModif.at(i));

        removeItem(myModif.at(i));
    }
    movement << doubleEaten;
    movement << pieceMoved;
    movement.prepend(myModif.at(1));
    movement.prepend(myModif.at(0));
    movHistory.append(movement);
    qDebug() << movement;
}
void GameData::goBack(){
    if(movHistory.size()>0){
        undoMovement();
        if(!vsHuman) undoMovement();
    }
}

void GameData::undoMovement()
{
    QList<qint8> lastMovement = movHistory.last();
    int initialState = lastMovement.last();
    int player = (initialState>0) ? 1 : -1;

    removeItem(lastMovement.at(1));
    ChessBox * destination = m_boxes.at(lastMovement.at(0));
    destination->setState(initialState);
    destination->setFlipped(true);
    int firstDoubleEatenIndex = lastMovement.size()-2-lastMovement.at(lastMovement.size()-2);

    for(int i=2; i<firstDoubleEatenIndex; ++i) {
        m_boxes.at(lastMovement.at(i))->setState(-player);
        m_boxes.at(lastMovement.at(i))->setFlipped(true);
    }
    for(int i=firstDoubleEatenIndex; i<lastMovement.size()-2; ++i){
        m_boxes.at(lastMovement.at(i))->setState(-player*2);
        m_boxes.at(lastMovement.at(i))->setFlipped(true);
    }

    movHistory.removeLast();
}

QList<qint8> GameData::choseNextMovement(QList< QList<qint8> *> *mods)
{
    QList<qint8> vault;
    int alpha=-26;
    QList<qint8> positions;
    for(int i=0; i<mods->size(); ++i){
        Draughtboard newState;
        newState.copyState(&m_boxes);
        MinimaxNode newNode;
        newNode.setDraughtboard(&newState);
        newNode.addMovement(mods->at(i));
        //qDebug() << (turn>0);
        vault << newNode.getValue(m_depth-1, turn>0, -26, 26);
        //qDebug() << "movement:" << *(mods.at(i)) << "value:" << vault.at(i);
        //qDebug() << newNode.toString() << vault.at(i);
        if(alpha<=vault.at(i)){
            if(alpha==vault.at(i)) positions << i;
            else{
                positions.clear();
                positions << i;
            }
            alpha=vault.at(i);
        }
    }
    int npos = qrand()%positions.size();
    QList<qint8> chosenPosition = *(mods->at(positions.at(npos)));
    for(int i=0; i<mods->size(); ++i){
        //qDebug() << *mods.at(i) << vault.at(i);
        delete (*mods)[i];
    }

    return chosenPosition;
}

void GameData::humanAndComputerMovement(int sndIndex, int fstIndex)
{
    //get modifications to do to the draughtboard
    QList<qint8> myModif = gameLogic->humanMovement(fstIndex,sndIndex,&m_boxes);
    if(myModif.length()>0){ // length > 0 means that the movement is acceptable
        doModifications(myModif);
        if(GameSettings::Instance()->getVersusHuman()){
            turn = -turn;
            QList< QList<qint8> *> mods = gameLogic->nextMovementPossibilities(turn, &m_boxes);
            /*
            for(int i=0; i<mods.size(); ++i){
                Draughtboard newState;
                newState.copyState(&m_boxes);
                MinimaxNode newNode;
                newNode.setDraughtboard(&newState);
                newNode.addMovement(mods.at(i));
                qDebug() << "movement:" << *(mods.at(i)) << "value:" << newNode.getValue(9, turn>0, -26,26);
            }
            */
            if(mods.size()==0) setGameState(-turn);
        }else{
            QList< QList<qint8> *> mods = gameLogic->nextMovementPossibilities(-turn, &m_boxes);
            if(mods.size()==0) setGameState(-turn);
            else if(mods.size()==1) doModifications(*(mods.at(0)));
            else{
                QTime time;
                time.start();
                QList<qint8> chosenPosition = choseNextMovement(&mods);
                qDebug() << "elapsed:"<<time.elapsed();
                //qDebug() << "n:" << npos << "size:" << positions.size();
                doModifications(chosenPosition);
            }
            mods = gameLogic->nextMovementPossibilities(turn, &m_boxes);
            if(mods.size()==0) setGameState(-turn);
            for(int i=0; i<mods.size(); ++i) delete mods[i];
        }
    }
}

void GameData::selectItem(int index){
    ChessBox *newSelected = getBox(index);
    if(!selected_boxes.contains(newSelected)){
        if(selected_boxes.length()==0 && newSelected->getState()*turn>0 && newSelected->isFlipped()){
            newSelected->setSelection(true);
            selected_boxes << newSelected;
        }else{if(selected_boxes.length()==1){
                if(!newSelected->isFlipped()){
                    newSelected->setSelection(true);
                    selected_boxes << newSelected;
                    //get absolute index of selected boxes
                    int firstIndex = m_boxes.indexOf(selected_boxes.at(0));
                    int secondIndex = m_boxes.indexOf(selected_boxes.at(1));
                    humanAndComputerMovement(secondIndex, firstIndex);
                    selected_boxes.at(0)->setSelection(false);
                    selected_boxes.at(1)->setSelection(false);
                    selected_boxes.clear();
                }else if(newSelected->getState()*turn>0){
                    //clicking two times on boxes with piece deselect the first and select the second
                    selected_boxes.at(0)->setSelection(false);
                    selected_boxes.clear();
                    newSelected->setSelection(true);
                    selected_boxes << newSelected;
                }
            }
        }
    }else{ //clicking two times on the same box deselect it
        selected_boxes.at(0)->setSelection(false);
        selected_boxes.clear();
    }
}

void GameData::moveItem(int sourceIndex, int destinationIndex){
    ChessBox *source = getBox(sourceIndex);
    ChessBox *destination = getBox(destinationIndex);
    int sourceState = source->getState();
    if(sourceState<0 && destinationIndex/BOX_COLS==0) sourceState = -2;
    if(sourceState>0 && destinationIndex/BOX_COLS==7) sourceState = 2;
    source->setFlipped(false);
    destination->setState(sourceState);
    destination->setFlipped(true);
}

void GameData::removeItem(int index){
    ChessBox *toRemove = getBox(index);
    toRemove->setFlipped(false);
}
void GameData::setGameDepth(int depth){setDepth(depth);}
void GameData::setVsHuman(bool vsHuman){GameSettings::Instance()->setVsHuman(vsHuman);}
