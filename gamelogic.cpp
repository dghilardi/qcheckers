//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "gamelogic.h"

GameLogic::GameLogic(QList<ChessBox *> *boxes, int _trow, int _tcols)
{
    tot_cols = _tcols;
    tot_row = _trow;
    m_boxes = boxes;
    singleCanEatDouble=false;

}

QList<qint8> GameLogic::humanMovement(int sourceindex, int destinationindex, QList<ChessBox *> * draughtboardState){
    Draughtboard situation;
    situation.copyState(draughtboardState);
    return NextMovements::getMovementModif(sourceindex, destinationindex, &situation);
}

QList<QList<qint8> *> GameLogic::nextMovementPossibilities(int player, QList<ChessBox *> * draughtboardState){
    Draughtboard situation;
    situation.copyState(draughtboardState);
    //qDebug() << "gameLogic nextmovs:" << situation.toString();
    return NextMovements::nextPossibilities(player, &situation);
}
