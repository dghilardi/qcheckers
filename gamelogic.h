/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMELOGIC_H
#define GAMELOGIC_H
#include <QList>
#include<QDebug>
#include <QString>
#include "chessbox.h"
#include "draughtboard.h"
#include "eating.h"
#include "nextmovements.h"

class GameLogic
{
public:
    GameLogic(QList<ChessBox *> *boxes, int _trow, int _tcols);
    QList<qint8> humanMovement(int sourceindex, int destinationindex, QList<ChessBox *> * draughtboardState);
    QList<qint8> computeNextMovement(int depth);
    bool canHumanMove();
    int checkModifiedDraughtboard(int destination, QList<qint8> previous, QList<qint8> alreadyEaten);
    QList<QList<qint8> *> nextMovementPossibilities(int player, QList<ChessBox *> *draughtboardState);
private:
    QList<ChessBox *> *m_boxes;
    int tot_row, tot_cols;
    bool singleCanEatDouble;
    QString listToString(QList<qint8> list);
    QString indexToCoordinate(int index);
    QString checkboardToString();
    QList<qint8> canEat(int position, QList<qint8> *previous);
    bool possibleEat(int source, int destination, QList<qint8> previous);
    QList<QList<qint8> *> eatingPossibilities(int source);
    QList<QList<qint8> *> getAllEatingPossibilities(int player);
    qint8 getDoubleNumber(QList<qint8> * movement);
    QList<QList<qint8> *> getBestEatingPossibilities(int player);
    qint8 getMaxNumber(QList<qint8> * intList);
    void removeMinorEat(QList<QList<qint8> *> *allPossibilities);
    void removeMinorDoubleEaters(QList<QList<qint8> *> *allPossibilities);
    qint8 getFirstDoubleEater(QList<qint8> * movement);
    qint8 getMinNumber(QList<qint8> * intList);
    void removeLaterDoubleEaters(QList<QList<qint8> *> *allPossibilities);
    QList<qint8> possibleMovement(int source);
    QList<QList<qint8> *> allPossibleMovements(int player);

    int abs(int a);
};

#endif // GAMELOGIC_H
