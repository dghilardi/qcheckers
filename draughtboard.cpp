//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "draughtboard.h"
#include <QDebug>

#define TOT_COLS 8
#define TOT_ROWS 8
#define BLACK_BOXES 32

Draughtboard::Draughtboard()
{
    draughtboard.resize(3*BLACK_BOXES);
    //qDebug() << "instantiated";
}

Draughtboard::Draughtboard(QString base64)
{
    //draughtboard.resize(3*BLACK_BOXES);
    //qDebug() << "instantiated";
    setBase64(base64);
}

void Draughtboard::setBase64(QString base64){
    QByteArray tmp;
    draughtboard=bytesToBits(QByteArray::fromBase64(base64.toAscii()));
    draughtboard.resize(3*BLACK_BOXES);
}


int Draughtboard::boxState(int position){
    int state=1;
    int index = toIndex(position);
    if(index!=-1){
        if(draughtboard.at(3*index+2)) state++;
        if(draughtboard.at(3*index+1)) state=-state;
        if(!draughtboard.at(3*index)) state=0;
    }else{
        state=0;
        //qDebug() << "attention! trying to read a whitebox state";
    }
    //QString text; for(int i=0; i<draughtboard.size(); ++i) text+=draughtboard.testBit(i) ? "1" : "0";
    //qDebug() << "draughtboard:" << text;
    //qDebug() << "state:" << state;
    return state;
}

void Draughtboard::setBoxState(int state, int position){
    //qDebug() << "set state:" << state << "position:" << position;
    int index = toIndex(position);
    if(index!=-1){
        //qDebug() <<"setting..." << index << position;
        if(state==0) draughtboard[3*index]=false; else draughtboard[3*index]=true;
        if(state <0) draughtboard[3*index+1]=true;  else draughtboard[3*index+1]=false;
        if(abs(state)==2) draughtboard[3*index+2]=true; else draughtboard[3*index+2]=false;
    }else{
        //qDebug() << "attention trying to set a whitebox";
    }
}

void Draughtboard::copyState(QList<ChessBox *> * draughtboardState){
    for(int i=0; i<draughtboardState->size(); ++i){
        ChessBox * actualBox = draughtboardState->at(i);
        if(actualBox->isBlack()) setBoxState(actualBox->getVisibleState(), i);
    }
}

int Draughtboard::toIndex(int position){
    int index;

    if(!isBlack(position)){
        index = -1;
    }else{
        index = position/2;
    }

    return index;
}

int Draughtboard::abs(int num){
    if(num<0) return -num; else return num;
}

bool Draughtboard::isBlack(int position){
    int row = position/TOT_COLS;
    return row%2==position%2;
}

int Draughtboard::size(){
    return BLACK_BOXES*2;
}

QString Draughtboard::toString(){
    QString drbrdString;
    drbrdString.append("\n");
    for(int i=0; i<BLACK_BOXES*2; ++i){
        int state=boxState(i);
        char rapp;
        switch(state){
            case 1:
                rapp='o';
                break;
            case 2:
                rapp='O';
                break;
            case -1:
                rapp='x';
                break;
            case -2:
                rapp='X';
                break;
            default:
                rapp=' ';
                break;
        }
        drbrdString.append(QString("|%1|").arg(rapp));
        if(i%TOT_COLS==TOT_COLS-1) drbrdString.append("\n");
    }
    return drbrdString;
}

QString Draughtboard::toBase64(){
    return bitsToBytes(draughtboard).toBase64();
}

QByteArray Draughtboard::bitsToBytes(QBitArray bits) {
    QByteArray bytes;
    bytes.resize(bits.count()/8+1);
    bytes.fill(0);
    // Convert from QBitArray to QByteArray
    for(int b=0; b<bits.count(); ++b)
        bytes[b/8] = ( bytes.at(b/8) | ((bits[b]?1:0)<<(b%8)));
    return bytes;
}

QBitArray Draughtboard::bytesToBits(QByteArray bytes) {
    QBitArray bits(bytes.count()*8);
    // Convert from QByteArray to QBitArray
    for(int i=0; i<bytes.count(); ++i)
        for(int b=0; b<8; ++b)
            bits.setBit(i*8+b, bytes.at(i)&(1<<b));
    return bits;
}
