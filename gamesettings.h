/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H
#include <QObject>

class GameSettings : public QObject
{
    Q_OBJECT

    public:
       static GameSettings* Instance();

       Q_PROPERTY(bool getSingleCanEatDouble READ getSingleCanEatDouble WRITE setSingleCanEatDouble NOTIFY singleCanEatDoubleChanged);
       Q_PROPERTY(bool getVersusHuman READ getVersusHuman WRITE setVsHuman NOTIFY vsHumanChanged);

       int getColumnNumber(){return tot_cols;}
       bool getSingleCanEatDouble(){return singleCanEatDouble;}
       bool getVersusHuman(){return versusHuman;}

       void setVsHuman(bool _vsHuman){if(versusHuman==_vsHuman) return; versusHuman=_vsHuman; emit vsHumanChanged();}
       void setSingleCanEatDouble(bool _singleCanEatDouble){if(singleCanEatDouble==_singleCanEatDouble) return; singleCanEatDouble = _singleCanEatDouble; emit singleCanEatDoubleChanged();}
    signals:
       void singleCanEatDoubleChanged();
       void vsHumanChanged();
    private:
       GameSettings(): tot_cols(8), tot_rows(8), singleCanEatDouble(false), versusHuman(false) {};  // Private so that it can  not be called
       GameSettings(GameSettings const&){};             // copy constructor is private
       GameSettings& operator=(GameSettings const&){};  // assignment operator is private
       static GameSettings* m_pInstance;
       int tot_cols, tot_rows;
       bool singleCanEatDouble;
       bool versusHuman;
};

#endif // GAMESETTINGS_H
