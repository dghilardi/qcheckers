//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "eating.h"

Eating::Eating()
{
}
/*
 * From a given source and destination of a piece return the consequences on the draughtboard.
 * If the movement is not allowed the list returned will be empty.
 * Otherwise the first two elements will be the source and the destination of the movement (the same as input), the other elements represents the positions of the pieces eaten in the movement (pieces to remove).
 */
QList<qint8> Eating::humanMovement(int sourceindex, int destinationindex, Draughtboard * situation){
    int sourceState = situation->boxState(sourceindex);
    int player = sourceState/abs(sourceState);
    QList<qint8> movement;
    QList<QList<qint8> *> nextMovPossibilities = nextMovementPossibilities(player, situation);
    for(int i=0; i<nextMovPossibilities.size(); i++){
        if(nextMovPossibilities.at(i)->at(0)==sourceindex && nextMovPossibilities.at(i)->at(1)==destinationindex){
            movement = *(nextMovPossibilities.at(i));
            nextMovPossibilities.removeAt(i);
            break;
        }
    }
    for(int i=0; i<nextMovPossibilities.size(); i++) delete nextMovPossibilities[i]; //controllare delete
    return movement;
}

QList<QList<qint8> *> Eating::nextMovementPossibilities(int player, Draughtboard * situation){
    QList<QList<qint8> *> eatingPossibilities = getBestEatingPossibilities(player, situation);
    if(eatingPossibilities.size()>0) return eatingPossibilities;
    else return allPossibleMovements(player, situation);
}

void Eating::removeMinorEat(QList<QList<qint8> *> *allPossibilities)
{
    QList<qint8> size;
    int maxSize;
    for(int i=0; i< allPossibilities->size(); ++i) size << allPossibilities->at(i)->size();
    //qDebug() << size;
    maxSize = getMaxNumber(&size);
    for(int i=allPossibilities->size()-1; i>=0; --i) if(allPossibilities->at(i)->size()!=maxSize){
        delete (*allPossibilities)[i];
        allPossibilities->removeAt(i);
    }
}

//keep only movements that eat much doubles
void Eating::removeMinorDoubleEaters(QList<QList<qint8> *> *allPossibilities, Draughtboard * situation)
{
    QList<qint8> doubleEaten;
    int maxNumDoubleEaten;
    for(int i=0; i< allPossibilities->size(); i++) doubleEaten << getDoubleNumber(allPossibilities->at(i), situation);
    //qDebug() << doubleEaten;
    maxNumDoubleEaten = getMaxNumber(&doubleEaten);
    for(int i=allPossibilities->size()-1; i>=0; i--) if(getDoubleNumber(allPossibilities->at(i),situation)!=maxNumDoubleEaten){
        delete (*allPossibilities)[i];
        allPossibilities->removeAt(i);
    }
}

//keep only movements that eat first doubles
void Eating::removeLaterDoubleEaters(QList<QList<qint8> *> *allPossibilities, Draughtboard * situation){
    QList<qint8> posDoubleEaten;
    int minPosDoubleEaten;
    int possibilitiesNumber = allPossibilities->size();
    for(int i=0; i< possibilitiesNumber; ++i) posDoubleEaten << getFirstDoubleEater(allPossibilities->at(i), situation);
    //qDebug() << posDoubleEaten;
    minPosDoubleEaten = getMinNumber(&posDoubleEaten);

    for(int i=possibilitiesNumber-1; i>=0; --i) if(getFirstDoubleEater(allPossibilities->at(i), situation)!=minPosDoubleEaten){
        delete (*allPossibilities)[i];
        allPossibilities->removeAt(i);
    }
}

QList<QList<qint8> *> Eating::getBestEatingPossibilities(int player, Draughtboard * situation){
    QList<QList<qint8> *> allPossibilities = getAllEatingPossibilities(player, situation);
    removeMinorEat(&allPossibilities);
    removeMinorDoubleEaters(&allPossibilities, situation);
    removeLaterDoubleEaters(&allPossibilities, situation);
    return allPossibilities;
}

qint8 Eating::getMaxNumber(QList<qint8> * intList){
    qint8 maximum=0;
    for(int i=0; i<intList->size(); i++){
        if (intList->at(i)>maximum) maximum=intList->at(i);
    }
    return maximum;
}

qint8 Eating::getMinNumber(QList<qint8> * intList){
    qint8 minimum;
    if(intList->size()>0) minimum=intList->at(0);
    for(int i=0; i<intList->size(); i++){
        if (intList->at(i)<minimum) minimum=intList->at(i);
    }
    return minimum;
}


//count numbers of double eaten in a movement
qint8 Eating::getDoubleNumber(QList<qint8> * movement, Draughtboard * situation){
    qint8 nDouble=0;
    int movementSize = movement->size();
    for(int i=2; i<movementSize; ++i){
        if(abs(situation->boxState(movement->at(i)))==2) {
            nDouble++;
        }
    }
    return nDouble;
}

/*
 * Get the position in the movement where a double is eaten
 */
qint8 Eating::getFirstDoubleEater(QList<qint8> * movement, Draughtboard * situation){
    qint8 DoublePos=0;
    int movementSize = movement->size();
    for(int i=2; i<movementSize; ++i){
        if(abs(situation->boxState(movement->at(i)))==2){
            DoublePos=i;
            break;
        }
    }
    return DoublePos;
}

/*
 * From a int representing the player and a situation of the draughtboard return all the possible player's movements that
 * eat at least one piece.
 */
QList<QList<qint8> *> Eating::getAllEatingPossibilities(int player, Draughtboard * situation){
    QList<QList<qint8> *> fullList;
    int boxNumber = situation->size();
    for(int i=0; i<boxNumber; ++i){
        if(situation->boxState(i)*player>0){
            QList<QList<qint8> *> eatinglist = eatingPossibilities(i, situation);
            if(eatinglist.at(0)->at(0)!=eatinglist.at(0)->at(1)) fullList.append(eatinglist);
            else for(int j=0; j<eatinglist.size(); ++j) delete eatinglist[j];
        }
    }
    //for(int i=0; i<fullList.size(); ++i) qDebug() << * fullList.at(i);
    return fullList;
}

/*
 * Return all possible eating of a given piece (passed by the position on draughtboard).
 * if given piece can't eat return a list that contain one list with first two elements equals.
 * otherwise return a list that contains another list with all the possible movement eating rival pieces
 * (takes only eatings with maximum number of pieces eaten).
 */
QList<QList<qint8> *> Eating::eatingPossibilities(int source, Draughtboard * situation){
    QList<QList<qint8> *> possibilitiesList;

    //initialize list
    possibilitiesList << new QList<qint8>;
    *possibilitiesList[0] << source << source;

    bool end;

    //while the last position permit at least one more eating continue add the new eating and remove the old
    do{
        end=true;
        int actualsize=possibilitiesList.size(); //---------vedere se è possibile rimuovere il while mettendo il for con solo il size
        for(int i=0; i<actualsize; i++){
            int firstPos = possibilitiesList.at(i)->at(1); // get position of destination
            QList<qint8> newPositions = canEat(firstPos, possibilitiesList.at(i), situation);
            //qDebug() << "->" << newPositions << "first:" << firstPos;
            if(newPositions.size()>0){
                end=false;
                //from the list of destination get the movement and insert in the list
                int numberOfPossibleDestination = newPositions.size();
                for(int j=0;j<numberOfPossibleDestination; j++){
                    QList<qint8> * newEaten = new QList<qint8>;
                    possibilitiesList << newEaten;
                    *newEaten << possibilitiesList.at(i)->at(0) << newPositions.at(j);
                    newEaten->append(possibilitiesList.at(i)->mid(2));
                    *newEaten << (possibilitiesList.at(i)->at(1) + newPositions.at(j))/2;
                    //qDebug() << *newEaten;
                }
                //remove old movement
                delete possibilitiesList[i];
                possibilitiesList.removeAt(i);
            }
        }
    }while(!end);
    return possibilitiesList;
}

QList<qint8> Eating::canEat(int position, QList<qint8> *previous, Draughtboard * situation){
    int tot_cols = GameSettings::Instance()->getColumnNumber();
    int column = position%tot_cols;
    int row = position/tot_cols;
    QList<qint8> finalPositions;
    int possibleDestinations[] = {
        (row+2)*tot_cols+column+2,
        (row+2)*tot_cols+column-2,
        (row-2)*tot_cols+column+2,
        (row-2)*tot_cols+column-2};
    for(int i=0; i<4; i++) if(possibleEat(position, possibleDestinations[i], *previous, situation)) finalPositions << possibleDestinations[i];
    return finalPositions;
}

/*
 * Check the destination state looking the state of the draughtboard as the pieces already eaten
 */
int Eating::checkModifiedDraughtboard(int destination, QList<qint8> previous, QList<qint8> alreadyEaten, Draughtboard * situation){
    int destinationState;
    if(alreadyEaten.contains(destination) || previous.at(0)==destination) destinationState = 0;
    else destinationState = situation->boxState(destination);

    return destinationState;
}

/*
 * Check if the passed eat (source and destination) is allowed.
 */
bool Eating::possibleEat(int source, int destination, QList<qint8> previous, Draughtboard * situation){
    //qDebug() << source << destination << previous;
    int tot_cols = GameSettings::Instance()->getColumnNumber();
    int sourceRow = source/tot_cols;
    int destinationRow = destination/tot_cols;
    QList<qint8> alreadyEaten = previous.mid(2);

    if(abs(sourceRow-destinationRow)!=2) return false;
    if(destination>=situation->size() || destination < 0) return false; //destination doesn't exist

    int destinationState = checkModifiedDraughtboard(destination, previous, alreadyEaten, situation);
    if(destinationState!=0)return false; //destination not free

    int direction = destinationRow-sourceRow;

    int type = situation->boxState(previous.at(0));
    int typeEaten=checkModifiedDraughtboard((source+destination)/2, previous, alreadyEaten, situation);
    if(abs(type)==1 && direction*type<0) return false;

    if(type*typeEaten<0){
        if(abs(type)>=abs(typeEaten)) {//qDebug() <<"ok:"<< source << destination;
            return true;}
        if(abs(type)==1 && abs(typeEaten)==2 && GameSettings::Instance()->getSingleCanEatDouble()) return true;
    }

    return false;
}

QList<QList<qint8> *> Eating::allPossibleMovements(int player, Draughtboard * situation){
    QList<QList<qint8> *> allMovements;
    int boxNumber = situation->size();
    for(int i=0; i<boxNumber; ++i){
        int type = situation->boxState(i);
        if(type*player>0){
            QList<qint8> pieceMovements = possibleMovement(i, situation);
            //qDebug() << pieceMovements;

            int numberOfMovements = pieceMovements.size();
            for(int j=0; j<numberOfMovements; ++j){
                QList<qint8> * movementslist = new QList<qint8>;
                *movementslist << i;
                *movementslist << pieceMovements.at(j);
                allMovements << movementslist;
            }
        }
    }
    return allMovements;
}

QList<qint8> Eating::possibleMovement(int source, Draughtboard * situation){
    QList<qint8> possibleDestinationsList;
    int tot_cols=GameSettings::Instance()->getColumnNumber();
    int boxNumber = situation->size();
    int row = source/tot_cols;
    int column = source%tot_cols;
    int type = situation->boxState(source);
    int possibleDestinations[] = {
        (row+1)*tot_cols+column+1,
        (row+1)*tot_cols+column-1,
        (row-1)*tot_cols+column+1,
        (row-1)*tot_cols+column-1};
    for(int i=0; i<4; i++){
        int destinationRow = possibleDestinations[i]/tot_cols;
        int direction = destinationRow-row;
        if(     possibleDestinations[i]>=0 && possibleDestinations[i]<boxNumber && //check overflows
                (direction*type>0 || abs(type)==2) && //single can only move in one direction, double in both
                situation->boxState(possibleDestinations[i]) == 0 && //destination must be free
                abs(direction)==1 //check row overflow
                ) possibleDestinationsList << possibleDestinations[i];
    }
    return possibleDestinationsList;

}

int Eating::abs(int a){
    if(a<0) return -a;
    else return a;
}
