//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "minimaxnode.h"
#define TOT_COLS 8
#define TOT_ROWS 8
#define BLACK_BOXES 32
MinimaxNode::MinimaxNode()
{
}

MinimaxNode::MinimaxNode(QString base64)
{
    draughtboardState.setBase64(base64);
}

int MinimaxNode::EuristicVault(){
    int value=0;
    for(int i=0; i<BLACK_BOXES*2; i++) value+=draughtboardState.boxState(i);
    return value;
}

void MinimaxNode::setDraughtboard(Draughtboard *state){
    draughtboardState=*state;
}

void MinimaxNode::addMovement(QList<qint8> *movement){
    int movementSize=movement->size();
    int destination=movement->at(1);
    int sourceState = draughtboardState.boxState(movement->at(0));
    draughtboardState.setBoxState(0, movement->at(0));
    for(int i=2; i<movementSize;++i){
        draughtboardState.setBoxState(0, movement->at(i));
    }
    if(destination/TOT_COLS==0 && sourceState==-1) sourceState=-2;
    if(destination/TOT_COLS==7 && sourceState==1) sourceState=2;
    draughtboardState.setBoxState(sourceState, destination);

}

int MinimaxNode::getValue(int depth, bool isMaxPlayer, int alpha, int beta){
    //qDebug() << (isMaxPlayer ? "max" : "min") << toString() << "depth:"<<depth << EuristicVault();
    int value, tmpValue;
    int player = isMaxPlayer ? 1 : -1;
    if(!NextMovements::isNextMovementPossible(player, &draughtboardState)) value = -player*(25+depth);
    else{
        if(depth==0){
            value = EuristicVault();
        }else{
            QList<QList<qint8> *> nextMovements = Eating::nextMovementPossibilities(player, &draughtboardState);
            if(isMaxPlayer){
                for(int i=0; i<nextMovements.size();++i){
                    MinimaxNode son = *this;
                    son.addMovement(nextMovements.at(i));
                    tmpValue = son.getValue(depth-1,!isMaxPlayer,alpha,beta);
                    alpha = alpha>tmpValue ? alpha : tmpValue;
                    if(beta<=alpha) break;
                }
                value = alpha;
            }else{
                for(int i=0; i<nextMovements.size(); ++i){
                    MinimaxNode son = *this;
                    son.addMovement(nextMovements.at(i));
                    tmpValue = son.getValue(depth-1,!isMaxPlayer,alpha,beta);
                    beta = beta<tmpValue ? beta : tmpValue;
                    if(beta<=alpha) {
                        //qDebug() << "break at depth:" << depth;
                        break;
                    }
                }
                value=beta;
            }
            for(int i=0; i<nextMovements.size(); i++) delete nextMovements[i];
        }
    }
    //qDebug() << (isMaxPlayer ? "max:" : "min:") << depth << "value:" << value << "base64:" << draughtboardState.toBase64();
    return value;
}

QString MinimaxNode::toString(){
    return draughtboardState.toString().append(draughtboardState.toBase64());
}

QString MinimaxNode::getBase64(){
    return draughtboardState.toBase64();
}
