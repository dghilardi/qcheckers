//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include <QApplication>
#include <QDeclarativeItem>
#include <QUrl>
#include <QTimer>
#include <QApplication>
#include <QDeclarativeEngine>
#include<QtDeclarative>
#include<QDebug>
#include "mainwidget.h"
#include "chessbox.h"
#include "qmlapplicationviewer.h"
#include "mainwidget.h"
#include "gamesettings.h"
#include <inneractiveplugin.h>
Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);


    qmlRegisterType<ChessBox>("gameCore", 1, 0, "Box");
    GameData m_gameData;
    QDeclarativeContext *m_context;
    m_context = viewer.rootContext();
    m_context->setContextProperty("mainWidget", &viewer);
    m_context->setContextProperty("gameData", &m_gameData);
    m_context->setContextProperty("gameSettings", GameSettings::Instance());
    inneractivePlugin::initializeEngine(viewer.engine());

    viewer.setMainQmlFile(QLatin1String("qml/QCheckers/main.qml"));
    viewer.showExpanded();

    return app->exec();
/*
    QApplication app(argc, argv);
    MainWidget mainWidget;
    mainWidget.show();
    return app.exec();*/
}
