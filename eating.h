/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EATING_H
#define EATING_H
#include <QDebug>
#include "draughtboard.h"
#include "gamesettings.h"

class Eating
{
public:
    static QList<qint8> humanMovement(int sourceindex, int destinationindex, Draughtboard * situation);
    static QList<qint8> computeNextMovement(int depth);
    static bool canHumanMove();
    static int checkModifiedDraughtboard(int destination, QList<qint8> previous, QList<qint8> alreadyEaten, Draughtboard * situation);
    static QList<QList<qint8> *> nextMovementPossibilities(int player, Draughtboard *situation);
private:
    static QString listToString(QList<qint8> list);
    static QString indexToCoordinate(int index);
    static QString checkboardToString();
    static QList<qint8> canEat(int position, QList<qint8> *previous, Draughtboard *situation);
    static bool possibleEat(int source, int destination, QList<qint8> previous, Draughtboard *situation);
    static QList<QList<qint8> *> eatingPossibilities(int source, Draughtboard *situation);
    static QList<QList<qint8> *> getAllEatingPossibilities(int player, Draughtboard *situation);
    static qint8 getDoubleNumber(QList<qint8> * movement, Draughtboard *situation);
    static QList<QList<qint8> *> getBestEatingPossibilities(int player, Draughtboard *situation);
    static qint8 getMaxNumber(QList<qint8> * intList);
    static void removeMinorEat(QList<QList<qint8> *> *allPossibilities);
    static void removeMinorDoubleEaters(QList<QList<qint8> *> *allPossibilities, Draughtboard *situation);
    static qint8 getFirstDoubleEater(QList<qint8> * movement, Draughtboard *situation);
    static qint8 getMinNumber(QList<qint8> * intList);
    static void removeLaterDoubleEaters(QList<QList<qint8> *> *allPossibilities, Draughtboard *situation);
    static QList<qint8> possibleMovement(int source, Draughtboard *situation);
    static QList<QList<qint8> *> allPossibleMovements(int player, Draughtboard *situation);

    static int abs(int a);
    Eating();
};

#endif // EATING_H
