/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMEDATA_H
#define GAMEDATA_H


#include <QObject>
#include <QString>
#include <QList>
#include <QStack>
#include <QTime>
#include <QTimer>
#include <QDeclarativeListProperty>
#include "chessbox.h"
#include "gamelogic.h"
#include "gamesettings.h"

class GameData : public QObject
{
    Q_OBJECT

public:
    GameData(QObject *parent=0);
    ~GameData();

    Q_PROPERTY(QDeclarativeListProperty<ChessBox> boxes READ boxes CONSTANT);
    QDeclarativeListProperty<ChessBox> boxes() {return QDeclarativeListProperty<ChessBox>(this, m_boxes);}

    Q_PROPERTY(int getGameState READ getGameState WRITE setGameState NOTIFY gameStateChanged);
    int getGameState() const {return m_gameState;}

    Q_PROPERTY(int getDepth READ getDepth WRITE setDepth NOTIFY depthChanged);
    int getDepth() const {return m_depth;}

    void setGameState(int state) { if(state==m_gameState) return; m_gameState = state; emit gameStateChanged(); }
    void setDepth(int depth) { if(depth==m_depth) return; m_depth = depth; emit depthChanged(); }

    ChessBox * resetBox(int j, int i);
    void doModifications(QList<qint8> myModif);

    Q_INVOKABLE void selectItem(int index);
    Q_INVOKABLE void newGame();
    Q_INVOKABLE void setGameDepth(int depth);
    Q_INVOKABLE void goBack();
    Q_INVOKABLE void setVsHuman(bool vsHuman);
    QList<qint8> choseNextMovement(QList<QList<qint8> *> *mods);
signals:
    void gameStateChanged();
    void depthChanged();
/*public slots:
    void selectItem(int index);
    void newGame();
*/
private:
    ChessBox *getBox(int index) const {return (index >= 0 && index < m_boxes.count()) ? m_boxes.at(index) : 0;}

    QList<ChessBox *> m_boxes;
    QList<ChessBox *> selected_boxes;
    QList<QList<qint8> > movHistory;
    int m_gameState;
    int m_depth;
    int turn; //-1 black turn, +1 white
    bool vsHuman;
    void removeItem(int index);
    void moveItem(int sourceIndex, int destinationIndex);
    void resetBox(int index);
    void humanAndComputerMovement(int secondIndex, int firstIndex);
    void undoMovement();
    GameLogic *gameLogic;
};


#endif // GAMEDATA_H
