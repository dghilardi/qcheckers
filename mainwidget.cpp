//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include <QDeclarativeItem>
#include <QUrl>
#include <QTimer>
#include <QApplication>
#include <QDeclarativeEngine>
#include<QtDeclarative>
#include<QDebug>
#include "mainwidget.h"
#include "chessbox.h"

QString filename("qml/QCheckers/main.qml");

MainWidget::MainWidget(QWidget *parent)
    : QDeclarativeView(parent)
{
    setResizeMode(QDeclarativeView::SizeRootObjectToView);

    // Register ChessBox to be available in QML
    qmlRegisterType<ChessBox>("gameCore", 1, 0, "Box");

    // Setup context
    m_context = rootContext();
    m_context->setContextProperty("mainWidget", this);
    m_context->setContextProperty("gameData", &m_gameData);

    // Set view optimizations not already done for QDeclarativeView
    setAttribute(Qt::WA_OpaquePaintEvent);
    setAttribute(Qt::WA_NoSystemBackground);

    // Open root QML file
    setSource(QUrl(filename));
}

MainWidget::~MainWidget()
{
}

void MainWidget::exitApplication()
{
    QApplication::quit();
}
