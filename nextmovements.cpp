//
// QCheckers - Checkers
// Copyright (C) 2013 ghilardi.davide@gmail.com
//
// This file is part of QCheckers.
//
// QCheckers is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// QCheckers is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
//

#include "nextmovements.h"

NextMovements::NextMovements()
{
}

QList<qint8> NextMovements::getMovementModif(int sourceindex, int destinationindex, Draughtboard * situation){
    int sourceState = situation->boxState(sourceindex);
    int player = sourceState/abs(sourceState);
    QList<qint8> movement;
    QList<QList<qint8> *> nextMovPossibilities = Eating::nextMovementPossibilities(player, situation);
    for(int i=0; i<nextMovPossibilities.size(); i++){
        if(nextMovPossibilities.at(i)->at(0)==sourceindex && nextMovPossibilities.at(i)->at(1)==destinationindex){
            movement = *(nextMovPossibilities.at(i));
            break;
        }
    }
    for(int i=0; i<nextMovPossibilities.size(); i++) delete nextMovPossibilities[i];
    return movement;
}

bool NextMovements::isNextMovementPossible(int player, Draughtboard *situation){
    QList<QList<qint8> *> nextPossibilities = Eating::nextMovementPossibilities(player, situation);
    bool existsMovements = nextPossibilities.size()>0;
    for(int i=0; i<nextPossibilities.size(); ++i) delete nextPossibilities[i];
    return existsMovements;
}

QList<QList<qint8> *> NextMovements::nextPossibilities(int player, Draughtboard *situation){
    return Eating::nextMovementPossibilities(player, situation);
}
