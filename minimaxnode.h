/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MINIMAXNODE_H
#define MINIMAXNODE_H
#include <QBitArray>
#include <QList>
#include "chessbox.h"
#include "draughtboard.h"
#include "nextmovements.h"

class MinimaxNode
{
public:
    MinimaxNode();
    MinimaxNode(QString base64);
    void setDraughtboard(Draughtboard *state);
    void addMovement(QList<qint8> *movement);
    int getValue(int depth, bool isMaxPlayer, int alpha, int beta);
    QString toString();
    QString getBase64();
private:
    Draughtboard draughtboardState;

    int EuristicVault();
};

#endif // MINIMAXNODE_H
