/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDeclarativeView>
#include <QDeclarativeContext>
#include "gamedata.h"

const QString contentPath = "content/";

class MainWidget : public QDeclarativeView
{
    Q_OBJECT

public:
    MainWidget(QWidget *parent=0);
    ~MainWidget();

public slots:
    void exitApplication();

private:
    QDeclarativeContext *m_context;
    GameData m_gameData;
};

#endif // MAINWIDGET_H
