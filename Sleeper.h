/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SLEEPER_H
#define SLEEPER_H
#include <QThread>

class Sleeper : public QThread
{
public:
    static void sleep(unsigned long secs) {QThread::sleep(secs);}
    static void msleep(unsigned long msecs) {QThread::msleep(msecs);}
    static void usleep(unsigned long usecs) {QThread::usleep(usecs);}
};
#endif // SLEEPER_H
