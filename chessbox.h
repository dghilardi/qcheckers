/* * QCheckers - Checkers
 * Copyright (C) 2013 ghilardi.davide@gmail.com
 *
 * This file is part of QCheckers.
 *
 * QCheckers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QCheckers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QCheckers.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOX_H
#define BOX_H
#include <QObject>

class ChessBox : public QObject
{
    Q_OBJECT

public:
    ChessBox(QObject *parent = 0) : QObject(parent),
        m_state(0), m_selected(false) {}

    Q_PROPERTY(int getState READ getState WRITE setState NOTIFY stateChanged);
    int getState() const { return m_state; }

    Q_PROPERTY(bool isSelected READ isSelected WRITE setSelection NOTIFY selectionChanged);
    bool isSelected() const { return m_selected; }

    Q_PROPERTY(bool isBlack READ isBlack WRITE setBlackBox NOTIFY blackChanged);
    bool isBlack() const { return m_blackbox;}

    Q_PROPERTY(bool isFlipped READ isFlipped WRITE setFlipped NOTIFY flippedChanged);
    bool isFlipped() const {return m_flipped;}

    void setState(int state) { if(state==m_state) return; m_state = state; emit stateChanged(); }
    void setSelection(bool state) { if(state==m_selected) return; m_selected = state; emit selectionChanged(); }
    void setBlackBox(bool bBox) {if(bBox==m_blackbox) return; m_blackbox = bBox; emit blackChanged();}
    void setFlipped(bool flipped) {if(flipped==m_flipped)return; m_flipped = flipped; emit flippedChanged();}

    int getVisibleState(){if(m_flipped) return m_state; else return 0;}

signals:
    void stateChanged();
    void selectionChanged();
    void blackChanged();
    void flippedChanged();

private:
    bool m_blackbox;
    int m_state;
    bool m_selected;
    bool m_flipped;

};
#endif // BOX_H
