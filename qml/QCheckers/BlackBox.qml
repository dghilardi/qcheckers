// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Flipable {
    id: flipable
    width: 240
    height: 240

    property int angle: 0
    property double selOpacity: 0
    property int state: 0
    property bool flipped: false
    property bool selected: false
    property bool mAreaEn: true
    property string emptyBox: "img/box_empty_black.svg.png"
    property string singleWhite: "img/box_single_white.svg.png"
    property string doubleWhite: "img/box_double_white.svg.png"
    property string singleBlack: "img/box_single_black.svg.png"
    property string doubleBlack: "img/box_double_black.svg.png"
    property string selectionImg: "img/select.svg.png"
    //property alias frontImageSrc: frontImage.source
    property string backImageSrc: (state == 1) ? singleWhite :
                                  (state == 2) ? doubleWhite :
                                  (state ==-1) ? singleBlack :
                                  (state ==-2) ? doubleBlack : emptyBox


    front: Item{
        anchors.fill: parent
        Image {
            id: frontSelection
            smooth: true
            anchors.fill: parent
            source: selectionImg
            opacity: selOpacity
        }
        Image {
            id: frontImage
            smooth: true
            anchors.fill: parent
            source: emptyBox }
    }

    back: Item{
        anchors.fill: parent
        Image {
            id: backSelection
            smooth: true
            anchors.fill: parent
            source: selectionImg
            opacity: selOpacity
        }
        Image {
            id: backImage
            smooth: true
            anchors.fill: parent
            source: backImageSrc }
    }


    transform: Rotation {
        origin.x: flipable.width/2; origin.y: flipable.height/2
        axis.x: 0; axis.y: 1; axis.z: 0     // rotate around y-axis
        angle: flipable.angle
    }

    states:[
        State {
            name: "backsel"
            PropertyChanges { target: flipable; angle: 180 }
            PropertyChanges{target: flipable; selOpacity:1}
            when: flipable.flipped && flipable.selected
        },
        State {
            name: "frontsel"
            PropertyChanges { target: flipable; angle: 0 }
            PropertyChanges{target: flipable; selOpacity:1}
            when: !flipable.flipped && flipable.selected
        },
        State {
            name: "frontnotsel"
            PropertyChanges { target: flipable; angle: 0 }
            PropertyChanges {target: flipable; selOpacity:0}
            when: !flipable.selected && !flipable.flipped
        },
        State{
            name: "backnotsel"
            PropertyChanges { target: flipable; angle: 180 }
            PropertyChanges {target: flipable; selOpacity:0}
            when: !flipable.selected && flipable.flipped
        }

    ]

    transitions:[
        Transition {
            PropertyAnimation{
                properties:"angle"
                duration: 5000
                easing.type: Easing.OutElastic
            }
            PropertyAnimation{
                properties: "selOpacity"
                duration: 2000
                easing.type: Easing.OutQuart
            }
        }
    ]
    MouseArea {
        acceptedButtons: "LeftButton"
        anchors.fill: parent
        //onClicked: flipable.selected = !flipable.selected
        enabled: mAreaEn
        onClicked: {
            gameData.selectItem(index);
        }
    }
}
