// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Item {
    property int pad: 4
    property string whiteBox: "img/box_empty_white.svg"
    id: container
    Grid {
        id: boardGrid
        columns: 8

        Repeater {
            model: gameData.boxes
            GenericBox {
                height: container.height/8
                width: container.height/8
            }
        }
    }
    WinnerImg{
        anchors.centerIn: boardGrid
        height: 1.05*container.height
        width: 1.05*container.height
    }
}
