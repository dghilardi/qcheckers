import QtQuick 1.1

Flipable {
    id: mySwitch
    width: 200
    height: 100
    property bool flipped: false
    property int angle: 0

    front:
        Image {
            id: frontImg
            smooth: true
            anchors.fill: parent
            source: "img/switch_off.png"
        }
    back:
        Image {
            id: backImg
            smooth: true
            anchors.fill: parent
            source: "img/switch_on.png"
        }
    transform: Rotation {
        id: rotation
        origin.x: mySwitch.width/2; origin.y: mySwitch.height/2
        axis.x: 0; axis.y: 1; axis.z: 0     // rotate around y-axis
        angle: mySwitch.angle
    }

    states: State {
        name: "back"
        PropertyChanges { target: rotation; angle: 180 }
        when: mySwitch.flipped
    }

    transitions:[
        Transition {
            PropertyAnimation{
                target: rotation
                properties:"angle"
                duration: 3000
                easing.type: Easing.OutBounce
            }
        }
    ]

    MouseArea {
        anchors.fill: parent
        onClicked: {
            //mySwitch.flipped =!mySwitch.flipped;
            onClick();
        }
    }
    function onClick(){}
}
