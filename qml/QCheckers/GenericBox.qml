// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Flipable {
    id: genericBox
    property bool flipped: modelData.isBlack
    property alias blackState: blackBox.state
    property alias blackFlip: blackBox.flipped


    front: Image { source: "img/box_empty_white.svg.png"; anchors.fill: parent; smooth: true }
    back: Item{
        anchors.fill: parent
        BlackBox{
        id: blackBox
        anchors.fill: parent
        flipped: modelData.isFlipped
        state: modelData.getState
        selected: modelData.isSelected
        mAreaEn: gameData.getGameState == 0
    }
    Text{
        text: index
        anchors.centerIn:  parent
        visible: flipped
    }
}

    transform: Rotation {
        id: rotation
        origin.x: genericBox.width/2
        origin.y: genericBox.height/2
        axis.x: 0; axis.y: 1; axis.z: 0 // set axis.y to 1 to rotate around y-axis
        angle: 0 // the default angle
    }

    states: State {
        name: "back"
        PropertyChanges { target: rotation; angle: 180 }
        when: genericBox.flipped
    }

    transitions: Transition {
        NumberAnimation { target: rotation; property: "angle"; duration: 0 }
    }
/*
    MouseArea {
        anchors.fill: parent
        onClicked: flipable.flipped = !flipable.flipped
    }*/
}
