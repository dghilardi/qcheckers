import QtQuick 1.1

Item {
    id: cpmain
    width: 500
    height: 500

    Column{
        Row{
            height: cpmain.height/8
            Image{
                width: cpmain.width*3/4
                height: parent.height
                source: "img/canEatDouble.png"
                smooth: true
            }

            MySwitch{
                width: cpmain.width/4
                height: parent.height
                flipped: gameSettings.getSingleCanEatDouble
                function onClick(){
                    gameSettings.getSingleCanEatDouble = !gameSettings.getSingleCanEatDouble
                }
            }
        }
        Row{
            height: cpmain.height/8
            Image{
                width: cpmain.width*3/4
                height: parent.height
                source: "img/vshuman.png"
                smooth: true
            }
            MySwitch{
                width: cpmain.width/4
                height: parent.height
                id: switchVsPc
                flipped: gameSettings.getVersusHuman
                function onClick(){
                    gameSettings.getVersusHuman = !gameSettings.getVersusHuman
                }

            }
        }
    }
}
