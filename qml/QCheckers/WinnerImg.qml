// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: winnerContainer
    property int imgHeight: 0
    property alias imgWidth: winnerImg.width
    property int gameState: gameData.getGameState
    Image{
        id: winnerImg
        smooth: true
        anchors.centerIn: parent
        source: (gameState == 1) ? "img/background_win_white.png" : (gameState == -1) ? "img/background_win_black.png" : ""
        height: imgHeight
        width: 0

    }

    RestartButton{
        anchors.horizontalCenter: parent.horizontalCenter
        y: winnerImg.height*4/5
        height: imgHeight*1/10
        width: imgWidth*2/5
        textSize: imgWidth*3/50+1
    }

    states:[
        State {
            name: "endGame"
            PropertyChanges { target: winnerContainer; imgHeight: winnerContainer.height*1 }
            PropertyChanges { target: winnerContainer; imgWidth: winnerContainer.height*1 }
            when: gameState != 0
        },
        State {
            name: "gameCourse"
            PropertyChanges { target: winnerContainer; imgHeight: 0 }
            PropertyChanges { target: winnerContainer; imgWidth: 0 }
            when: gameState == 0
        }
    ]
    transitions:
        Transition {
            PropertyAnimation{
                properties:"imgHeight, imgWidth"
                duration: 2000
                easing.type: Easing.OutBounce
            }
        }
}
