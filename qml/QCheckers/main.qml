// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "slider"
Item {
    id: mainItem
    width: 720
    height: 1280
    Image {
        id: background
        source: "img/background6.png"
        property int h: parent.height
        height: h
        width: (sourceSize.width*h)/sourceSize.height
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Row{
        id: row
        property int lftSpace: mainItem.width - draughtboard.width - ctrlpanel.width
        property int minhw: mainItem.height < mainItem.width ? mainItem.height : mainItem.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        height: 0.9*minhw
        spacing: lftSpace/5
        //width: 0.9*mainItem.height
        Flipable{
            id: flipableDraughtboard
            property bool flipped: false
            height: parent.height; width: parent.height
            front: Draughtboard {
                id: draughtboard
                height: parent.height
                width: parent.height
                //anchors.horizontalCenter: parent.horizontalCenter
                //anchors.verticalCenter: parent.verticalCenter
            }
            back: ControlPanel{
                height: parent.height; width: parent.height

                }
            transform: Rotation {
                id: rotation
                origin.x: flipableDraughtboard.width/2
                origin.y: flipableDraughtboard.height/2
                axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
                angle: 0    // the default angle
            }
            states: State {
                name: "back"
                PropertyChanges { target: rotation; angle: 180 }
                when: flipableDraughtboard.flipped
            }

            transitions: Transition {
                NumberAnimation { target: rotation; property: "angle"; duration: 4000 }
            }

        }
        Item{
            id: ctrlpanel
            property int h2: parent.height
            //property real proportions: hal.height/hal.sourceSize.height
            height: h2
            //width: (hal.sourceSize.width*h2)/hal.sourceSize.height
            width: btnundo.width
            visible: (parent.spacing>0)
            /*Image{
                id: hal
                anchors.fill: parent
                source: "img/hal.jpg"
                smooth: true
            }
            MySlider{
                x: 12*parent.proportions
                y: 695*parent.proportions
                proportion: parent.proportions
            }*/
            Column{
                GraphicButton{
                    id: btnundo
                    height: ctrlpanel.h2/4
                    width: ctrlpanel.h2/4
                    function onClick(){gameData.goBack();}
                }
                GraphicButton{
                    id: btoptions
                    imageSource: "img/gears.png"
                    height: ctrlpanel.h2/4
                    width: ctrlpanel.h2/4
                    function onClick(){flipableDraughtboard.flipped=!flipableDraughtboard.flipped;}
                }
            }
        }
    }

}
