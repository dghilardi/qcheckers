// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Item{
    id: rotator
    property real proportion: 1
    property real lastValue: (gameData.getDepth*(3/2)*Math.PI)/nLevels
    property alias dragActive: m_area.pressed
    property alias mAngle: m_area.angle
    property int nLevels: 1
    property real barAngle: 0
    property real normalizedRotation: barAngle > 270 ? 270 : barAngle <0 ? 0 : barAngle
    property int actualDepth: (lastValue/(Math.PI*3/2))*(nLevels-1)+1
    width: 169*proportion
    height: 169*proportion
    Text{
        anchors.centerIn: parent
        font.pointSize: 90*proportion
        font.family: "ubuntu"
        text: gameData.getDepth
    }

    Item {
        id: rotation_bar
        anchors.centerIn: parent
        width: 169*proportion
        height: 32*proportion
        rotation: -normalizedRotation
        Cursor {
            id: cursor
            proportion: rotator.proportion
            anchors.horizontalCenter: rotation_bar.right
            rotation: -rotation_bar.rotation
            is_active: dragActive
        }

    }
    MouseArea{
        id: m_area
        anchors.fill: parent
        property real cMouseY: -mouseY+height/2
        property real cMouseX: mouseX-width/2
        property real angle: 2*Math.atan(cMouseY/(Math.sqrt(cMouseX*cMouseX+cMouseY*cMouseY)+cMouseX))
        onReleased: {
            rotator.lastValue = Math.round(rotator.normalizedRotation*(rotator.nLevels-1)/270)*(3/2)*Math.PI/(rotator.nLevels-1)
            gameData.setGameDepth(actualDepth)
        }
    }
    states:[
        State {
            name: "onDragging"
            PropertyChanges { target: rotator; barAngle: mAngle>=-Math.PI/4 ? mAngle*360/(2*Math.PI) : mAngle*360/(2*Math.PI)+360 }
            when: dragActive
        },
        State {
            name: "stopped"
            PropertyChanges { target: rotator; barAngle: lastValue>=0 ? (lastValue*360)/(2*Math.PI) : (lastValue*360)/(2*Math.PI)+360}
            when: !dragActive
        }
    ]

    transitions:[
        Transition {
            from: "stopped"
            to: "onDragging"
            PropertyAnimation{
                properties:"barAngle"
                duration: 200
                easing.type: Easing.OutQuad
            }
        },
        Transition {
            from: "onDragging"
            to: "stopped"
            PropertyAnimation{
                properties:"barAngle"
                duration: 1000
                easing.type: Easing.OutElastic
            }
        }
    ]
}
