// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: cursor
    width: 32*proportion
    height: 32*proportion
    property real proportion: 1
    property double selOpacity: 0
    property bool is_active: false
    Image{
        height: sourceSize.height*proportion
        width: sourceSize.width*proportion
        smooth: true
        source: "img/cursor_off.png"
    }
    Image{
        id: hovImg
        height: sourceSize.height*proportion
        width: sourceSize.width*proportion
        smooth: true
        source: "img/cursor_on.png"
        //visible: mouse_area.containsMouse
        opacity: selOpacity
    }

    states:[
        State {
            name: "btn_on"
            PropertyChanges { target: cursor; selOpacity: 1.0 }
            when: cursor.is_active
        },
        State {
            name: "btn_off"
            PropertyChanges { target: cursor; selOpacity: 0.0 }
            when: !cursor.is_active
        }
    ]

    transitions:
        Transition {
            PropertyAnimation{
                properties:"selOpacity"
                duration: 1000
                easing.type: Easing.OutQuad
            }
        }
}
