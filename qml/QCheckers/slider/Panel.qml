// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Flipable {
    id: flipable
    width: 318*proportion
    height: 209*proportion
    property real proportion: 1
    property bool flipped: false
    front: Image {
        Image{
            height: sourceSize.height*proportion
            width: sourceSize.width*proportion
            smooth: true
            source: "img/background.png"
        }
        Rotator{
            id: rotator
            x: 21*proportion
            y: 21*proportion
            proportion: flipable.proportion
            nLevels: 10
        }
    }
    back: Image {}

    transform: Rotation {
        id: rotation
        origin.x: flipable.width
        origin.y: 0
        axis.x: 1; axis.y: 0; axis.z: 0     // set axis.y to 1 to rotate around y-axis
        angle: 90    // the default angle
    }

    states:[
        State {
                name: "front"
                PropertyChanges { target: rotation; angle: 90 }
                when: flipable.flipped
            },
        State {
        name: "back"
        PropertyChanges { target: rotation; angle: 0 }
        when: !flipable.flipped
    }]

    transitions: [
        Transition {
            from: "back"
            to: "front"
            PropertyAnimation{
                target: rotation
                properties:"angle"
                duration: 800
                easing.type: Easing.OutQuad
            }
        },
        Transition {
        PropertyAnimation{
            target: rotation
            property: "angle"
            duration: 2000
            easing.type: Easing.OutBounce
        }
    }]
}
