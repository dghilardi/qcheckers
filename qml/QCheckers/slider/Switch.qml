// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: mySwitch
    property bool on: false
    property real proportion: 1
    width: 32*proportion
    height: 48*proportion
    Image{
        x: 4*proportion
        height: sourceSize.height*proportion
        width: sourceSize.width*proportion
        smooth: true
        source: "img/rail.png"
    }
    Image{
        id: cursor
        height: sourceSize.height*proportion
        width: sourceSize.width*proportion
        y: -3*proportion
        smooth: true
        source: "img/cursor_off.png"
    }
    states:
        State{
        name: "btn_on"
        PropertyChanges { target: cursor; y: 19*proportion }
        when: mySwitch.on
    }
    transitions:
        Transition {
            PropertyAnimation{
                target: cursor
                properties:"y"
                duration: 100
                easing.type: Easing.OutQuad
            }
        }

    MouseArea{
        anchors.fill: parent
        onClicked: mySwitch.on = !mySwitch.on
    }
}
