// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
Item{
    id: mainItem
    height:209
    width: 318
    property real proportion: height/209
    /*Image{
        height: sourceSize.height*proportion
        width: sourceSize.width*proportion
        source: "img/background_hal.png"
    }*/
Switch{
    id: mySwitch
    proportion: mainItem.proportion
    x: 282*proportion
    y: 157*proportion
}

Panel{
    proportion: mainItem.proportion
    flipped: !mySwitch.on
}}
