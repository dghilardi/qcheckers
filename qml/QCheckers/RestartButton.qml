// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: container
    radius: 5
    property double opacity_item: 0
    color: Qt.rgba(1,1,1,0.2+0.2*opacity_item)
    property int textSize: 30

    Text{
        smooth: true
        text: "Restart"
        opacity: 0.4+0.6*opacity
        font.pointSize: textSize
        visible: font.pointSize>2
        font.family: "ubuntu"
        anchors.centerIn: parent
    }
    MouseArea{
        id: m_area
        anchors.fill: parent
        hoverEnabled: true
        onClicked:{
            gameData.newGame();
        }
    }
    states:[
        State {
            name: "hover_on"
            PropertyChanges { target: container; opacity_item:1 }
            when: m_area.containsMouse
        },
        State {
            name: "hover_off"
            PropertyChanges { target: container; opacity_item:0 }
            when: !m_area.containsMouse
        }
    ]

    transitions:
        Transition {
            PropertyAnimation{
                properties:"opacity_item"
                duration: 1000
                easing.type: Easing.InOutCubic
            }
        }
}
