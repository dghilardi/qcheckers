import QtQuick 1.1

Rectangle {
    id: container
    radius: 5
    property double opacity_item: 0
    color: Qt.rgba(1,1,1,0.2*opacity_item)
    property int textSize: 30
    property string imageSource: "img/undo.png"
    function onClick(){}

    Image{
        height: parent.height-5
        width: parent.width -5
        source: imageSource
        smooth: true
        anchors.centerIn: parent
        opacity: 0.4+0.6*opacity
    }

    MouseArea{
        id: m_area
        anchors.fill: parent
        hoverEnabled: true
        onClicked:{
            onClick()
        }
    }
    states:[
        State {
            name: "hover_on"
            PropertyChanges { target: container; opacity_item:1 }
            when: m_area.containsMouse
        },
        State {
            name: "hover_off"
            PropertyChanges { target: container; opacity_item:0 }
            when: !m_area.containsMouse
        }
    ]

    transitions:
        Transition {
            PropertyAnimation{
                properties:"opacity_item"
                duration: 1000
                easing.type: Easing.InOutCubic
            }
        }
}
